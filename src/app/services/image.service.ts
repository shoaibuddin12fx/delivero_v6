import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AlertsService } from './basic/alerts.service';
import { ModalService } from './basic/modal.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(public alerts: AlertsService, private camera: Camera, public modals: ModalService) { }

  snapImage(type) {

    return new Promise(async (resolve) => {


      const radioOptions = [
        {
          type: 'radio',
          label: 'Camera',
          value: '1',
          checked: false
        },
        {
          type: 'radio',
          label: 'Gallery',
          value: '0',
          checked: false
        }
      ];

      const option = await this.alerts.presentRadioSelections('Select From', '', radioOptions);

      if (option == null) { resolve(null); return; }

      const options: CameraOptions = {
        quality: 100,
        targetWidth: 512,
        targetHeight: 512,
        saveToPhotoAlbum: false,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: parseInt(option, 10)
      };

      this.camera.getPicture(options).then((imageData) => {

        // if (imageData) {
          // this.cropWithController('data:image/jpeg;base64,' + imageData, type)
          //   .then(image =>
              resolve('data:image/jpeg;base64,' + imageData);
            // err => console.error(err))
        // }
      // });
      });

    });
  }

  convertImageUrltoBase64(url) {
    console.log('here-url-', url);

    return new Promise((resolve) => {
      if(!url){
        resolve(null);
      }else{
        if (!this.isValidUrl(url) || (/^http/.test(url))) {
          const index = url.lastIndexOf('/') + 1;
          const filename = url.substr(index);
          resolve(filename);
        } else {
          this.convertToDataURLviaCanvas(url).then(base64 => {
            resolve(this.getRB64fromB64(base64));
          });
        }
      }


    });
  }

  isValidUrl = (string) => {
    try {
      new URL(string);
      return true;
    } catch (_) {
      return false;
    }
  };

  getRB64fromB64(str) {
    return str.substring(str.indexOf(',') + 1);
  }

  convertToDataURLviaCanvas(url, outputFormat = 'image/jpeg') {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = () => {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS');
          const ctx = canvas.getContext('2d');
          let dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        resolve(dataURL);
        canvas = null;
      };
      img.src = url;
    });
  }



}
