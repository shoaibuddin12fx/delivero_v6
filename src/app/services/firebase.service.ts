import { Injectable } from '@angular/core';
import { FCM } from '@ionic-native/fcm/ngx';
import { AudioService } from './audio.service';
// import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { EventsService } from './events.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(public events: EventsService,
    public network: NetworkService,
    private fcm: FCM,
    public audio: AudioService,
    public sqlite: SqliteService
    ) {
      this.assignEvents();
    }

  assignEvents() {
    this.events.subscribe('user:settokentoserver', this.setTokenToServer.bind(this));
  }

  async setTokenToServer() {
    const fcm_token = await this.getFCMToken();
    console.log("fcm_token",fcm_token);
    if (fcm_token) {
      this.network.saveFcmToken({ token: fcm_token }).then(dats => {
      }, err => { console.error(err) });
    }

  }

  setupFMC() {

    this.fcm.subscribeToTopic('all');
    this.fcm.onNotification().subscribe(data => {
      if (!data.wasTapped) {
        this.audio.play("");
        if (data['showalert'] != null) {
          this.events.publish('user:shownotificationalert', data);
        } else {
          this.events.publish('user:shownotification', data);
        }
      };
    })
    this.fcm.onTokenRefresh().subscribe(token => {
      this.sqlite.setFcmToken(token);
      this.events.publish('user:settokentoserver');
    });

  }


  async getFCMToken() {
    return new Promise(resolve => {
      this.fcm.getToken().then(v => resolve(v), (err) => { console.log(err); resolve(null) }).catch(v => { console.log(v); resolve(null) })
    })
  }








}
