import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  audio: any;
  received = false;

  constructor(
    public network: NetworkService,
    public platform: Platform) {
    // console.log('Hello AudioProvider Provider');
  }


  preload(){

    // this.network.getSoundLocation().then( (data) => {
        this.received = true;
        this.audio = new Audio();
        this.audio.src =  '../../assets/sound/marimba.mp3'; //data.sound;
        this.audio.load();
    // }, err => {});
  }

  play(key: string): void {

    if(this.received === true){
      this.audio.play();
    }


  }
}
