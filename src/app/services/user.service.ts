import { Injectable } from '@angular/core';
import { EventsService } from './events.service';
import { FirebaseService } from './firebase.service';
import { NavService } from './nav.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
const sampleUser = require('./../data/user.json');

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // endpoint: string = Config.api + "/user";
  _user: any;
  avatar = sampleUser.avatar;

  constructor(
    public utilityProvider: UtilityService,
    public sqlite: SqliteService,
    public events: EventsService,
    public network: NetworkService,
    public nav: NavService,
    public firebaseService: FirebaseService,
  ) {



  }

  assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    // this.events.subscribe('user:login', this.login.bind(this));
    this.events.subscribe('user:get', this.getUser.bind(this));
    // this.events.subscribe('user:successpage', this.setSuccessPage.bind(this));
    // this.events.subscribe('user:shownotificationalert', this.notificationReceivedalert.bind(this));
    this.events.subscribe('user:setcontactstodatabase', this.setContacts.bind(this));

  }

  setContacts(user) {
    this.getContactDatabseOfUser(user).then(v => {
      console.log('import completed', v);
    });
  }

  // notificationReceivedalert(data) {
  //   // // console.log(data.showalert);
  //   if (data.hasOwnProperty('showalert')) {
  //     // this.rnotif = true;
  //     this.utilityProvider.showAlert(data.showalert).then(() => {
  //       if (data.showalert == 'Pass scanned successfully') {
  //         this.nav.setRoot('DashboardPage', {
  //           animate: true,
  //           direction: 'backword'
  //         });
  //       }
  //     });
  //   }
  // }



  // setSuccessPage(params) {
  //   //// console.log(params);
  //   // this.menuCtrl.enable(true, 'authenticated');
  //   this.nav.setRoot('SuccessPage', params);
  // }

  isAuthenticated() {
    // here you can check if user is authenticated or not through his token
    return new Promise( async resolve => {      
      resolve(this.network.getProfile());
    })

  }

  login(user) {

    console.log(user);

    this.network.login(user).then(async (res:any)=> {
      console.log(res);
      // localStorage.setItem('token',res.token)
        // this.utilityService.showToast(data.message, 'success');
        // localStorage.setItem('token', data.token);
        // console.log(data)
        // this.getUserProfileData();
        // resolve(userData);
      // } else {
      //   this.utilityService.showToast((data.message == 'No such user') ? (errorMessages.ERROR_SOCIAL_LOGIN) : data.message, 'error');
      //   reject(data.message);
      // }

      // const uuser = res.user;

      // // this.menuCtrl.enable(true, 'authenticated');
      // const token = res.token;
      // uuser.token = token;
      // uuser.active = 1;
      // await this.processUserData(uuser, false);

    }, err => { });

  }

  logout(sw = null) {
    // this.menuCtrl.enable(false, 'authenticated');
    this.sqlite.setLogout();
    if(!sw){
      this.nav.setRoot('pages/tutorial');
    }else{
      this.nav.setRoot('pages/login', {switchUser: true, phone_number: sw['phone_number']});
    }

  }

  public getActiveUser(){
    return this.sqlite.getActiveUser();
  }

  public getUser() {

    return new Promise(async resolve => {
      this.network.getUser().then(async (user: any) => {

        console.log('peel', user);
        user = user.user;
        if (user) {
          this.processUserData(user, false);
          resolve(user);
        } else {
          // redirect to steps
          this.logout();
        }
      }, err => {
        this.logout();
      });
    })

  }

  async setTokenToServer(){
    this.firebaseService.setTokenToServer();
  }

  async processUserData(user, showelcome) {

    // check if sqlite set already, if not fetch records
    // const _user = user
    console.log(user);

    user.fcm_token = await this.firebaseService.getFCMToken();
    // this.user_role_id = parseInt(_user['role_id']);
    // this.utilityProvider.setKey('user_role_id', this.user_role_id);
    const saveduser = await this.sqlite.setUserInDatabase(user);
    // this.menuCtrl.enable(true, 'authenticated');
    console.log(saveduser);

    if (!saveduser) {
      this.logout();
      return;
    }

    this.setUser(saveduser);
    this.nav.setRoot('pages/dashboard');
    // this.canBeResident = (parseInt(saveduser["can_user_become_resident"]) == 1);
    // this.canShowSettings = parseInt(saveduser["role_id"]) != 7

    // let currentUrl = this.nav.router.url;
    // console.log(currentUrl);

    // if (currentUrl == '/1/DashboardPage') {
    //   this.events.publish('dashboard:initialize');
    // } else {
    //   this.nav.setRoot('1/DashboardPage',
    //     {
    //       showelcome: showelcome,
    //       animate: true,
    //       direction: 'forward'
    //     }
    //   );
    // }




  }


  setUser(user) {
    this._user = user;
  }

  getCurrentUser() {
    return this._user;
  }

  update(data, token) {
    return {
      data,
      token
    };
  }

  switchUserAccount(swUser) {
    return new Promise<any>(async resolve => {
      await this.sqlite.switchLogin(swUser.id);
      const user = this.sqlite.getActiveUser();

      // this.events.publish('stored:resetvariables');
      // this.events.publish('user:get');
      resolve(user);
    });

  }

  public async getContactDatabseOfUser(user) {

    return new Promise(async resolve => {

      const count = await this.sqlite.getCurrentUserContactsCount(user.id);
      if (count == 0) {

        const msg = 'Preparing one time data ... please wait ';
        this.utilityProvider.presentToast(msg);
        // this.network.getOneTimeContactsData(false).then( async v => {
        //   console.log(v);

        //   await this.sqlite.setGroupListInDatabase(v["contact_group"]);
        //   await this.sqlite.setGroupCollectionInDatabase(v["contact_collection"]);
        //   await this.sqlite.setContactListInDatabase(v["contact_list"]);
        //   await this.sqlite.setEventListInDatabase(v["events"]);
        //   // await this.sqlite.setVendorListInDatabase([]);

        //   let contacts = await this.utilityProvider.getPhoneContacts();
        //   console.log(contacts);
        //   if(contacts){
        //     await this.sqlite.setSyncContactListInDatabase(contacts, true);
        //   }

        //   console.log("records inserted");
        //   // this.utilityProvider.hideLoader();
        //   this.utilityProvider.presentToast("records inserted");
        resolve(true);

        // })

      } else {
        resolve(false);
      }

    });
  }








}
