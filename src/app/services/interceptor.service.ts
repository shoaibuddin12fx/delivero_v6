import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageService } from './basic/storage.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor  {

  constructor(private sqlite: SqliteService, private storage: StorageService, public utility: UtilityService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.callToken()).pipe(
      switchMap(token => {
        const cloneRequest = this.addSecret(req, token );
        return next.handle(cloneRequest);
      })
    );

  }

  callToken(){
    return new Promise( resolve => {
      this.sqlite.getCurrentUserAuthorizationToken().then( v => {
        resolve(v);
      }).catch( err => {
        console.error(err);
        resolve(null);});
    });
  }

  private addSecret(request: HttpRequest<any>, value: any){
    const v = value ? value : '';
    const clone = request.clone({
      setHeaders : {
        Authorization: 'Bearer ' + v,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    });
    return clone;
  }
}
