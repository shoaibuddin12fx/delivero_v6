import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {

  loginForm: FormGroup;
  loginSuccessful: boolean = false;
  submitAttempt: boolean;

  constructor(injector: Injector) { 
    super(injector);

    // create form
    this.loginForm = new FormGroup({
      email: new FormControl('anupresy@gmail.com', [Validators.email, Validators.required]),
      password: new FormControl('@1981king', Validators.required)
    });
    
  }

  ngOnInit() {
  }


  onLogin(){
    this.submitAttempt = true;
    const formdata = this.loginForm.value;
    if(this.loginForm.valid && this.submitAttempt){
      this.users.login(formdata)
    }
  }

}
