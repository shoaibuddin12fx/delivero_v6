import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {

  logo = '/assets/images/logo.png';
  loading = false;
  constructor(injector: Injector) {
    super(injector);

    this.platform.ready().then(() => {
      this.init();
    });

  }

  ngOnInit() {

  }

  async init() {
    // get all data from local storage
    this.loading = true;
    await this.sqlite.initialize();
    const self = this;
    // setTimeout(() => {
      // self.callRedirect();
      self.loading = false;

      if (!(localStorage.getItem('introVisited'))) {
        this.nav.push('pages/tutorial');
        return;
      }

      if (localStorage.getItem('currentUser')) {
        const flag = await this.users.isAuthenticated();
        if (flag) {
          this.gotToUserRoleSelection(JSON.parse(localStorage.getItem('currentUser')));
        }
      }


    // }, 1200);
  }

  gotToUserRoleSelection(currentUser){

    if(currentUser){
      this.nav.push('pages/roleSelection')
    }
  }



}
