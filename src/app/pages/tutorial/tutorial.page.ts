import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage extends BasePage implements OnInit {

  @ViewChild('slides', {static: true}) slides: IonSlides;
  
  data = [
    {
      title: 'Place Order',
      buttonText: 'Next',
      subtitle: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      image: 'assets/images/intro1.png'
    },
    {
      title: 'Delivery',
      buttonText: 'Next',
      subtitle: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      image: 'assets/images/intro3.png'
    }
  ];

  constructor(injector: Injector) {
    super(injector)
  }

  ngOnInit() {
  }

  async goToNextSlide(){
    const isEnd = await this.slides.isEnd();
    console.log(isEnd);
    if(isEnd){
      localStorage.setItem('introVisited', 'true');
      this.nav.push('pages/login');
    }else{
      this.slides.slideNext();
    }
  }

}
