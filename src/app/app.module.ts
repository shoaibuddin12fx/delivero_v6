import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { ApiService } from './services/api.service';
import { StorageService } from './services/basic/storage.service';
import { EventsService } from './services/events.service';
import { InterceptorService } from './services/interceptor.service';
import { NetworkService } from './services/network.service';
import { SqliteService } from './services/sqlite.service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    SQLite,
    ImagePicker,
    LaunchNavigator,
    CallNumber,
    InAppBrowser,
    Camera,
    DatePicker,
    FirebaseX,
    Geolocation,
    SplashScreen,
    StatusBar,
    FCM,
    // custom providers
    NgxPubSubService,
    EventsService,
    SqliteService,
    StorageService,
    ApiService,
    NetworkService,
    Location,

  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
